# pomo

Minimalistic [pomodoro](https://en.wikipedia.org/wiki/Pomodoro_Technique) timer that is intended to be integrated using i3blocks or similar. 

## usage
See help:
```
$ ./pomo -h
usage: pomo [-h] {start,restart,stop} ...

positional arguments:
  {start,restart,stop}
    start               Start the timer
    restart             Restart the timer. If no session is active, a new
                        session is started
    stop                Stops a running pomodoro session

optional arguments:
  -h, --help            show this help message and exit

```

## example
```
$ pomo 
Inactive
$ pomo start -d 15
$ pomo
Active (14m57s)
$ pomo stop
$ pomo
Inactive
```

## how is this even useful? 
If you're trying to use this program without a third party program that is constantly
pulling the status it probably isn't. :-)
However, if you use an external program like i3blocks or similar, you can integrate it 
easily into your navigation bar:

![You should see a screenshot here](/doc/running.png?raw=true "pomo running in i3blocks")

Once your time is over, a notification is being sent using *notify-send*: 

![You should see a screenshot here](/doc/buzzing.png?raw=true "notification sent by pomo")

## i3blocks integration example
Add the following block to your i3blocks.conf file:
```
[pomo]
label=🍅
interval=15
```
After that, just create a file named *pomo* inside your ~/.i3blocks/ folder with the following content:
```sh
#!/bin/sh

ENTRIES=$(~/bin/pomo | cut -d'(' -f2 | cut -d')' -f1)

if [[ -z $ENTRIES ]]; then
    ENTRIES=Inactive
fi

echo $ENTRIES
echo $ENTRIES
```